/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef _HAL_MEASURE_H_
#define _HAL_MEASURE_H_

#ifdef __cplusplus
extern "C"
{
#endif   

#include "BaseTypes.h"

void HAL_Measure_Init();
void HAL_Measure_Start(void);
void HAL_Measure_Stop(void);
s16  HAL_Measure_PollAdc(u8 ucAdcChannel);


#ifdef __cplusplus
}
#endif    

#endif //_HAL_MEASURE_H_
