/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

#include "HAL_Measure.h"
#include "HAL_Config.h"
#include "Arduino.h"

#if (WITHOUT_REGULATION == false)
/****************************************** Defines ******************************************************/

/****************************************** Variables ****************************************************/
    
/****************************************** Function prototypes ******************************************/
static pFunctionParamU8S16 pValueCallbackFn = NULL;

/****************************************** loacl functiones *********************************************/

/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Initialize the measurement module.
\return     none
\param      none
***********************************************************************************/
void HAL_Measure_Init(pFunctionParamU8S16 pCallbackFn)
{
    if(pCallbackFn)
    {
        pValueCallbackFn = pCallbackFn; //Save callback function        
        HAL_Measure_Start();
    }
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       26.06.2021
\brief      Reads the ADC value of the ADC-register and when Callback-Fn is registered
            the ADC value is put into the buffer. Also returns the polled value
\return     siAdcVal - The read ADC value
\param      none
***********************************************************************************/
s16  HAL_Measure_PollAdc(u8 ucAdcChannel)
{
    /* Reads the ADC Channel */
    s16 siAdcVal = analogRead(ADC_INPUT_CHANNEL0);

    /* Get the ADC value and put it in the average buffer */   
    if(pValueCallbackFn) 
    {
        pValueCallbackFn(0, siAdcVal);
    }

    return siAdcVal;
}

#endif

#endif //ESPRESSIF_ESP8266