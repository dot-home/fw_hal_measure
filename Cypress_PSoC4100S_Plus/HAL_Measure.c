/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

#include "HAL_Measure.h"
#include "HAL_Config.h"
#include "project.h"

#if (WITHOUT_REGULATION == false)
/****************************************** Defines ******************************************************/

/****************************************** Variables ****************************************************/
    
/****************************************** Function prototypes ******************************************/
static void MeasureInterrupt(void);
static pFunctionParamU8S16 pValueCallbackFn = NULL;

/****************************************** loacl functiones *********************************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Interrupt function which should be called, whenever a ISR occurs.
\return     none
\param      none
***********************************************************************************/
static void MeasureInterrupt(void)
{    
    /* Clear isr flag */
    #if Measure_ISR    
        Measure_ISR_ClearPending();
    #else
        ADC_INPUT_IRQ_ClearPending();
        
        /* Read interrupt status register */
        u32 intr_status = ADC_INPUT_SAR_INTR_REG;
        
        /* Clear handled interrupt */
        ADC_INPUT_SAR_INTR_REG = intr_status;
    #endif
    
    /* Get the ADC value and put it in the average buffer */    
    pValueCallbackFn(AMuxSeq_GetChannel(), ADC_INPUT_GetResult16(ADC_INPUT_CHANNEL0));
    
    /* Switch multiplexer to the next channel */
    AMuxSeq_Next();
    
    /* Trigger the conversion */
    ADC_INPUT_StartConvert();
}

/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Initialize the measurement module.
\return     none
\param      none
***********************************************************************************/
void HAL_Measure_Init(pFunctionParamU8S16 pCallbackFn)
{
    #if Measure_ISR
        /* Set ISR address */
        Measure_ISR_StartEx(MeasureInterrupt);
        Measure_ISR_Enable();
    #else        
        if(pCallbackFn)
        {
            pValueCallbackFn = pCallbackFn; //Save callback function
        
            HAL_Measure_Start();
            ADC_INPUT_IRQ_StartEx(MeasureInterrupt);
            //ADC_INPUT_SAR_INTR_MASK_REG |= ADC_INPUT_EOS_MASK;
        }
        else
        {
            
        }
    #endif
}



//********************************************************************************
/*!
\author     Kraemer E.
\date       10.02.2019
\brief      Starts the ADC module.
\return     none
\param      none
***********************************************************************************/
void HAL_Measure_Start(void)
{
    // Start analog multiplexer. Starts with -1
    AMuxSeq_Start();
    while(AMuxSeq_GetChannel() < 0)
    {
        AMuxSeq_Next();
    }    
    
    // Set channel mask for all inputs
    ADC_INPUT_SetChanMask(0xFF);
    
    /* Enable ADC-Module */
    ADC_INPUT_Start();
    
    /* Enable the interrupt */
    //ADC_INPUT_IRQ_Enable();
    
    /* Start the first conversion. Every other conversion is
       started in the interrupt */
    ADC_INPUT_StartConvert();
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       10.02.2019
\brief      Stops the ADC module.
\return     none
\param      none
***********************************************************************************/
void HAL_Measure_Stop(void)
{
    // Set channel mask for no inputs
    ADC_INPUT_SetChanMask(0x00);
    
    /* Stops the ADC module */
    ADC_INPUT_Stop();
    //ADC_INPUT_IRQ_Disable();
    
    /* Stop Analog multiplexer */
    AMuxSeq_Stop();
}

#endif

#endif //PSOC_4100S_PLUS